library ieee;
use ieee.std_logic_1164.all;
use ieee.fixed_pkg.all;

entity PlantModelDriver is
	generic(numbits : integer;
			fracbits : integer);
	port(clk : in std_logic;
		 n_reset : in std_logic;
		 input : in sfixed(numbits - fracbits - 1 downto -fracbits);
		 output : out real);
end entity;

architecture behav of PlantModelDriver is
begin
	process(clk, n_reset, input) is
	begin
		if not n_reset then
			output <= 0.0;
		elsif rising_edge(clk) then
			output <= to_real(input);
		end if;
	end process;
end architecture;

