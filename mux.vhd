library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity mux is
	generic(BITWIDTH : integer);
	port(current_temp : in std_logic_vector(BITWIDTH-1 downto 0);
		 desired_temp : in std_logic_vector(BITWIDTH-1 downto 0);
		 display_select : in std_logic;
		 temp_display : out std_logic_vector(BITWIDTH-1 downto 0));
end entity;

architecture rtl of mux is
begin
	process (current_temp, desired_temp, display_select) is
	begin
		if display_select = '1' then
			temp_display <= current_temp;
		else
			temp_display <= desired_temp;
		end if;
	end process;
end architecture;
