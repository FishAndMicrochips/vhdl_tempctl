library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity Room is
	generic(ambient_K : real;
			sample_time : real;
			k1 : real;
			k2 : real);
	port(sample_clk : in std_logic;
		 n_reset : in std_logic;
		 input_power : in real;
		 output_temp : out real);
end entity;

architecture behav of Room is
begin
	process(sample_clk, n_reset, input_power) is
	begin
		if not n_reset then
			output_temp <= ambient_K;
		elsif rising_edge(sample_clk) then
			output_temp <= output_temp
						   + k2 * sample_time * input_power
						   - k1 * sample_time * (output_temp - ambient_K);
		end if;
	end process;
end architecture;
