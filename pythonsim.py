#!/usr/bin/env python

import numpy as np
from matplotlib import pyplot as plt

class FilterModel:
    def __init__(self, a, b, init_y=0):
        self.a = a
        self.b = b
        self.y_last = init_y

    def reset(self):
        self.y_last = 0

    def __call__(self, x):
        y = self.b * x + self.a * self.y_last
        self.y_last = y
        return y


def firstOrderModel(k1, k2, sample_T):
    b = k2 * (k1 * sample_T)
    # b = k2
    a = np.exp(-k1 * sample_T)
    return FilterModel(a, b)

def main():
    # sine_freq = 0.05  # hertz
    # n_cycles = 5
    # stop_time = float(n_cycles) / sine_freq
    stop_time = 5
    print(f'stop time = {stop_time}')
    sample_freq = 1000
    sample_T = 1/sample_freq
    num_pts = int(np.ceil(stop_time * sample_freq))
    time = np.linspace(0, stop_time, num_pts)
    #  xs = np.sin(2*np.pi*sine_freq*time)
    xs = [0] + [1] * (num_pts - 1)
    k1 = 1
    k2 = 2
    my_model = firstOrderModel(k1, k2, sample_T)

    ys = np.array([my_model(x) for x in xs])

    plt.figure()
    plt.plot(time, xs)
    plt.plot(time, ys)
    plt.xlabel("Time (s)")
    plt.ylabel("f(t)")
    plt.legend(["Input $x(t)$", "output $y(t)$"])
    plt.title("The response of the overall system to a sine input, low $f$.")
    plt.show()
