library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity SpaceHeater is
	generic(numbits : integer;
			maxpower : real);
	port(clk : in std_logic;
		 n_reset : in std_logic;
		 input : in std_logic_vector(numbits-1 downto 0);
		 output_kW : out real);
end entity;

architecture behav of SpaceHeater is
	constant maxval : std_logic_vector(numbits-1 downto 0) := (others => '1');
	signal input_signed : signed(numbits-1 downto 0) := to_signed(0, numbits);
begin
	input_signed <= signed(input);
	
	process(clk, n_reset, input) is
	begin
		if not n_reset then
			output_kW <= 0.0;
		elsif rising_edge(clk) then
			output_kW <= maxpower
						 * (real(to_integer(input_signed))
							/ real(to_integer(unsigned(maxval))))
						 + (maxpower/2.0);
		end if;
	end process;
end architecture;


