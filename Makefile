GHDL=ghdl
FLAGS=--std=08 -fsynopsys
SIMFLAGS=--wave=wave.ghw --stop-time=600000ms --disp-time
TOPLEVEL=MainTB
SRC=room.vhd space_heater.vhd temp_sensor.vhd controller.vhd main_tb.vhd

build:
	@$(GHDL) -a $(FLAGS) $(SRC)
	@$(GHDL) -e $(FLAGS) $(TOPLEVEL)

run:
	@$(GHDL) -r $(FLAGS) $(TOPLEVEL) $(SIMFLAGS)

all: build
