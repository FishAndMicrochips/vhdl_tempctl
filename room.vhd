library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity Room is
	generic(sample_time : real;
			initial_K : real);
	port(ambient_K : in real;
		 sample_clk : in std_logic;
		 n_reset : in std_logic;
		 input_power : in real;
		 output_temp : out real;		 
		 k1 : in real;
		 k2 : in real);
end entity;

architecture behav of Room is
begin
	process(sample_clk, n_reset, input_power) is
	begin
		if not n_reset then
			output_temp <= initial_K;
		elsif rising_edge(sample_clk) then
			output_temp <= output_temp
						   + k2 * sample_time * input_power
						   - k1 * sample_time * (output_temp - ambient_K);
		end if;
	end process;
end architecture;
