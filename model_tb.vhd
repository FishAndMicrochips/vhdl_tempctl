library ieee;
use ieee.std_logic_1164.all;
use ieee.math_real.all;

entity model_tb is
end entity;

architecture sim of model_tb is
	-- Constants
	constant clk_freq : integer := 10e6;
	constant clk_period : time := 1000 ms / clk_freq;
	constant k1 : real := 10000.0;
	constant k2 : real := 1.0;

	-- Clock and Reset
	signal input_power : real := 0.0;
	signal output_temp : real;
	signal clk : std_logic := '0';
	signal n_reset : std_logic := '0';
begin
	model : entity work.plant_model(behav)
		generic map(clock_freq => clk_freq,
					k1 => k1,
					k2 => k2)
		port map(clk => clk,
				 n_reset => n_reset,
				 input_power => input_power,
				 output_temp => output_temp);
	clk <= not clk after clk_period/2;
	n_reset <= '1' after 3 ns;
	input_power <= 10.0 after 2*clk_period;
	
end architecture;
