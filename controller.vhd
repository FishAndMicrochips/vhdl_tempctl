library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Controller is
	generic(numbits : integer);
	port(clk : in std_logic;
		 n_reset : in std_logic;
		 input_ref : in std_logic_vector(numbits-1 downto 0);
		 temp_sense : in std_logic_vector(numbits-1 downto 0);
		 output_word : out std_logic_vector(numbits-1 downto 0));
end entity;


architecture rtl of Controller is
	signal ref_signed : signed(numbits-1 downto 0) := to_signed(0, numbits);
	signal temp_signed : signed(numbits-1 downto 0) := to_signed(0, numbits);	
	signal out_signed : signed(numbits-1 downto 0) := to_signed(0, numbits);
	signal propterm : signed(numbits-1 downto 0) := to_signed(0, numbits);
	signal diffterm : signed(numbits-1 downto 0) := to_signed(0, numbits);
	constant out_min : signed(numbits-1 downto 0)
		:= to_signed(2 ** (numbits-1) - 1, numbits);
	constant out_max : signed(numbits-1 downto 0) := not out_min;
	signal new_out_signed : signed(numbits-1 downto 0)
		:= to_signed(0, numbits);
	signal new_diffterm : signed(numbits-1 downto 0)
		:= to_signed(0, numbits);
	signal new_propterm : signed(numbits-1 downto 0)
		:= to_signed(0, numbits);
	signal err : signed(numbits-1 downto 0) := to_signed(0, numbits);
begin
	ref_signed <= signed(input_ref);
	temp_signed <= signed(temp_sense);
	output_word <= std_logic_vector(out_signed);

	-- Make sure that the new value is capped
	new_propterm <= ref_signed - temp_signed;
	err <= shift_left(propterm, 2) + shift_left(diffterm, 5);
	new_out_signed <= out_signed + err;
	new_diffterm <= new_propterm - propterm;
	
	process (clk, n_reset, temp_sense, input_ref) is

	begin
		if not n_reset then
			out_signed <= to_signed(0, numbits);
			diffterm <= to_signed(0, numbits);
			propterm <= to_signed(0, numbits);
		elsif rising_edge(clk) then
			-- Raw error signal
			propterm <= new_propterm;
			if (err > 0 and new_out_signed > out_signed)
				or (err < 0 and new_out_signed < out_signed) then
				out_signed <= new_out_signed;
			end if;
			if (propterm < 0 and new_diffterm > diffterm)
				or (propterm > 0 and new_diffterm < diffterm) then
				diffterm <= new_diffterm;
			end if;	
		end if;
	end process;
end architecture;
