library ieee;
use ieee.std_logic_1164.all;
use ieee.math_real.all;
use ieee.fixed_pkg.all;

entity MainTB is
end entity;

architecture sim of MainTB is
	-- Constants
	constant sample_freq : integer := 1e3;
	constant sample_period : time := 1000 ms / sample_freq;
	constant sample_time_real : real := 1.0 / real(sample_freq);
	constant clk_freq : integer := 1;
	constant clk_period : time := 1000 ms / clk_freq;
	constant numbits : integer := 16;
	constant maxpower : real := 10.0;
	constant maxtemp : real := 308.15;

	-- Thermal signals
	signal k1 : real := 0.002; -- Loss due to leaks
	signal k2 : real := 0.01; -- Gain due to heat input
	signal initial_K : real := 283.15;
	signal ambient_K : real := 273.15;
	constant desired_temp : real := 287.15;

	-- Space Heater Signals
	signal space_heater_input_word : std_logic_vector(numbits-1 downto 0)
		:= (others => '0');
	signal space_heater_output_kW : real;

	-- Power sent into the room
	signal input_power_real : real := 0.0;
	signal room_temp : real;

	-- Sensor values
	signal sensor_reading : std_logic_vector(numbits-1 downto 0)
		:= (others => '0');

	-- Reference
	signal ref_word : std_logic_vector(numbits-1 downto 0)
		:= (others => '0');

	-- Clock and Reset
	signal clk : std_logic := '0';
	signal sample_clk : std_logic := '0';
	signal n_reset : std_logic := '0';
	
begin
	space_heater : entity work.SpaceHeater(behav)
		generic map(numbits => numbits,					
					maxpower => maxpower)
		port map(clk => clk,
				 n_reset => n_reset,
				 input => space_heater_input_word,
				 output_kW => space_heater_output_kW);
	
	room : entity work.Room(behav)
		generic map(sample_time => sample_time_real,
					initial_K => initial_K)
		port map(ambient_K => ambient_K,
				 sample_clk => sample_clk,
				 n_reset => n_reset,
				 input_power => space_heater_output_kW,
				 output_temp => room_temp,
				 k1 => k1,
				 k2 => k2);

	ref_generator : entity work.TempSensor(behav)
		generic map(numbits => numbits,
					mintemp => ambient_K,
					maxtemp => maxtemp)
		port map(clk => clk,
				 n_reset => n_reset,
				 input_temp => desired_temp,
				 output_word => ref_word);

	temp_sensor : entity work.TempSensor(behav)
		generic map(numbits => numbits,
					mintemp => ambient_K,
					maxtemp => maxtemp)
		port map(clk => clk,
				 n_reset => n_reset,
				 input_temp => room_temp,
				 output_word => sensor_reading);

	controller : entity work.Controller(rtl)
		generic map(numbits => numbits)
		port map(clk => clk,
				 n_reset => n_reset,
				 input_ref => ref_word,
				 temp_sense => sensor_reading,
				 output_word => space_heater_input_word);
	
	sample_clk <= not sample_clk after sample_period/2;
	clk <= not clk after clk_period/2;
	
	n_reset <= '1' after 3 ns;
	k1 <= 0.006 after 300000 ms; -- Open the window after 5 mins

end architecture;
