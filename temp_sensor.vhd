library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity TempSensor is
	generic(numbits : integer;
			maxtemp : real;
			mintemp : real);
	port(clk : in std_logic;
		 n_reset : in std_logic;
		 input_temp : in real;
		 output_word : out std_logic_vector(numbits-1 downto 0));
end entity;

architecture behav of TempSensor is
	constant temprange : real := maxtemp - mintemp;
	constant maxval : std_logic_vector(numbits-1 downto 0) := (others => '1');
	constant max_real : real := real(to_integer(unsigned(maxval)));
begin
	process (clk, n_reset, input_temp) is
		variable output_real : real := 0.0;
	begin
		if not n_reset then
			output_word <= (others => '0');
		elsif rising_edge(clk) then
			output_real := max_real * (input_temp - mintemp) / temprange;
			output_word <= std_logic_vector(to_unsigned(integer(output_real),
														output_word'length));
		end if;
	end process;
end architecture;
